﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace demoTranslatorChat.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(int users)
        {
            //if (Session["isreg"] != null && (bool)Session["isreg"])
            //{
            //    ViewBag.UsersCount = HttpContext.Application["users"];
            //}
            //else
            //{
            //    Session.Add("isreg",true);
            //    HttpContext.Application["users"] = (int)HttpContext.Application["users"] + 1;
            //    ViewBag.UsersCount = HttpContext.Application["users"];
            //}

            ViewBag.UsersCount = users;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}