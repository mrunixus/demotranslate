﻿using demoTranslatorChat.Models;
using demoTranslatorChat.Translator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace demoTranslatorChat.Controllers
{
    public class TranslateContentController : Controller
    {
        
        // GET: TranlateContent
        public ActionResult Index()
        {
            return View();
        }


        [HttpGet]
        public ViewResult Translate()
        {
            TranslateContentViewModel model = new TranslateContentViewModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ViewResult Translate(TranslateContentViewModel model) {

            TranslationHistoryViewModel tmpList = null;

            if (Session["TranslatedHistory"] != null)
            {
                tmpList = (TranslationHistoryViewModel)Session["TranslatedHistory"];
            }
            else
	        {
                    tmpList = new TranslationHistoryViewModel();
                    Session.Add("TranslatedHistory", tmpList);
	        }



            AdmAccessToken headerValue = null;

            if (Session["AdmToken"] == null || (Session["AdmToken"] != null && ((AdmAccessToken)Session["AdmToken"]).IsExpired()))
            {
                Session.Remove("AdmToken");
                headerValue = GetBearerTokenMSTranslator();
                Session.Add("AdmToken", headerValue);
            }
            else
            {
                headerValue = (AdmAccessToken)Session["AdmToken"];
            }
            

            //Required connection elements acquired.

            string txtToTranslate = model.TransalateContentModel.TextToTranslate;
            string uri = "http://api.microsofttranslator.com/v2/Http.svc/Translate?text=" +
                 System.Web.HttpUtility.UrlEncode(txtToTranslate) + "&from="+model.SelectedSourceLanguage+"&to="+model.SelectedDestLanguage;
            System.Net.WebRequest translationWebRequest = System.Net.WebRequest.Create(uri);
            translationWebRequest.Headers.Add("Authorization", headerValue.ToString());

            System.Net.WebResponse response = null;
            response = translationWebRequest.GetResponse();
            System.IO.Stream stream = response.GetResponseStream();
            System.Text.Encoding encode = System.Text.Encoding.GetEncoding("utf-8");

            System.IO.StreamReader translatedStream = new System.IO.StreamReader(stream, encode);
            System.Xml.XmlDocument xTranslation = new System.Xml.XmlDocument();
            xTranslation.LoadXml(translatedStream.ReadToEnd());

            model.TransalateContentModel.TranslatedText = xTranslation.InnerText;

            //model.TransalateContentModel.TranslatedText = model.TransalateContentModel.TextToTranslate + " Translated";

            tmpList.History.Add(model.TransalateContentModel);
            Session["TranslatedHistory"] = tmpList;
            return View(model);
        }

       


        public PartialViewResult HistoryTranslate() {
            TranslationHistoryViewModel tmpList = null;

            if (Session["TranslatedHistory"] != null)
            {
                tmpList = (TranslationHistoryViewModel)Session["TranslatedHistory"];
            }
            else
            {
                tmpList = new TranslationHistoryViewModel();
                Session.Add("TranslatedHistory", tmpList);
            }

            return PartialView(tmpList);
        }

        [Authorize]
        public ActionResult Chat()
        {
            return View();
        }

        private static AdmAccessToken GetBearerTokenMSTranslator()
        {
            string clientID = "demotranslatorpmorales";
            string clientSecret = "2ddVMNYRJl0QmlQfYfSmqQqykFmnRcainqfKDk0LecE=";
            String strTranslatorAccessURI =
                  "https://datamarket.accesscontrol.windows.net/v2/OAuth2-13";
            String strRequestDetails =
                  string.Format("grant_type=client_credentials&client_id={0}&client_secret={1} &scope=http://api.microsofttranslator.com", HttpUtility.UrlEncode(clientID),
                      HttpUtility.UrlEncode(clientSecret));

            System.Net.WebRequest webRequest = System.Net.WebRequest.Create(strTranslatorAccessURI);
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.Method = "POST";
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(strRequestDetails);
            webRequest.ContentLength = bytes.Length;
            using (System.IO.Stream outputStream = webRequest.GetRequestStream())
            {
                outputStream.Write(bytes, 0, bytes.Length);
            }

            System.Net.WebResponse webResponse = webRequest.GetResponse();
            System.Runtime.Serialization.Json.DataContractJsonSerializer serializer = new
                System.Runtime.Serialization.Json.DataContractJsonSerializer(typeof(AdmAccessToken));

            AdmAccessToken token =
                (AdmAccessToken)serializer.ReadObject(webResponse.GetResponseStream());
            token.Initalize();
            

            //string headerValue = "Bearer " + token.access_token;
            return token;
        }
    }
}