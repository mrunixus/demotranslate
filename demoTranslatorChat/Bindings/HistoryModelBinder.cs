﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace demoTranslatorChat.Bindings
{
    public class HistoryModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {

            //if (Session["isreg"] != null && (bool)Session["isreg"])
            //{
            //    ViewBag.UsersCount = HttpContext.Application["users"];
            //}
            //else
            //{
            //    Session.Add("isreg",true);
            //    HttpContext.Application["users"] = (int)HttpContext.Application["users"] + 1;
            //    ViewBag.UsersCount = HttpContext.Application["users"];
            //}

            if (controllerContext.HttpContext.Application["users"] == null)
            {
                controllerContext.HttpContext.Application["users"] = 0;
            }

            if (controllerContext.HttpContext.Session["isreg"] == null )
            {
                controllerContext.HttpContext.Application["users"] = (int)controllerContext.HttpContext.Application["users"] + 1;
                controllerContext.HttpContext.Session.Add("isreg", true);
            }

            return (int)controllerContext.HttpContext.Application["users"];

        }
    }
}