﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace demoTranslatorChat.Models
{
    public class TranslationHistoryViewModel
    {
        public List<TranslateContent> History { get; set; }

        public TranslationHistoryViewModel() { History = new List<TranslateContent>(); }
    }
}