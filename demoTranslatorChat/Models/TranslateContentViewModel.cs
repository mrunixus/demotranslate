﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace demoTranslatorChat.Models
{
    public class TranslateContentViewModel
    {

        public TranslateContent TransalateContentModel { get; set; }
        public IEnumerable<SelectListItem> SourceLanguage { get { return new SelectList(_languages, "Id", "Name"); } }
        public IEnumerable<SelectListItem> DestinationLanguage { get { return new SelectList(_languages, "Id", "Name"); } }

        public string SelectedSourceLanguage { get; set; }
        public string SelectedDestLanguage { get; set; }
        

        public TranslateContentViewModel() {
            _languages = new List<PerLanguage> { new PerLanguage { Id = "en", Name = "English" }, new PerLanguage { Id = "es", Name = "Spanish" },
                                                new PerLanguage{ Id = "it", Name = "Italian"}, new PerLanguage{ Id = "fr", Name = "French"}};

            this.SelectedDestLanguage = "es";

            

        }



        public System.Collections.IEnumerable _languages { get; set; }
    }


    public class PerLanguage {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}