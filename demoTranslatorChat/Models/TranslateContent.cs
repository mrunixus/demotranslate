﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace demoTranslatorChat.Models
{
    public class TranslateContent
    {
        [Display(Name="Text to Translate")]
        [DataType(DataType.MultilineText)]
        public string TextToTranslate { get; set; }

        [Display(Name="Translation")]
        public string TranslatedText { get; set; }
    }
}