﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace demoTranslatorChat.Hubs
{
    public class User
    {
        public string Name { get; set; }
        public string SourceLanguage { get; set; }
        public string DestLanguage { get; set; }
        public HashSet<string> ConnectionIds { get; set; }


        public User() {
            this.SourceLanguage = "en";
            this.DestLanguage = "es";
        }
    }
}