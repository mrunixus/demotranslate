﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace demoTranslatorChat.Translator
{
    public class AdmAccessToken
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public string expires_in { get; set; }
        public string scope { get; set; }

        public DateTime tokenEndTime { get; set; }

        public bool IsExpired()
        {
            //return false;
            DateTime now = DateTime.Now.AddSeconds(0);
            double secondsLeft = tokenEndTime.Subtract(now).TotalSeconds;
            if (secondsLeft < 30)
                return true;
            else
                return false;
        }

        public void Initalize()
        {
            tokenEndTime = DateTime.Now.AddSeconds(600) ;
        }

        public override string ToString()
        {
            return "Bearer " + access_token; 
        }
    }
}